# Shortcuts

Shortcuts is a GNOME Shell Extension that displays shortcut descriptions from a
json file when Ctrl + Alt + Super + S is pressed (hotkey can be changed in settings). The default keyboard shortcuts are listed
in the main package.

## Install

### Dependencies:

Building Shortcuts from source requires:

-   Glib development files (`libglib2.0-bin`)
-   `gettext`

### Install

    ./install.sh

You may need to restart GNOME (Alt + F2, r) before you see Shortcuts in your
list of extensions

# 1 click install from E.G.O:

[<img src="https://raw.githubusercontent.com/andyholmes/gnome-shell-extensions-badge/master/get-it-on-ego.svg" height="125">](https://extensions.gnome.org/extension/1144/shortcuts/)

# Screenshots

Popup

<img src="https://gitlab.com/paddatrapper/shortcuts-gnome-extension/-/raw/master/screenshots/popup.png?ref_type=heads">

Settings

<img src="https://gitlab.com/paddatrapper/shortcuts-gnome-extension/-/raw/master/screenshots/settings.png?ref_type=heads">
