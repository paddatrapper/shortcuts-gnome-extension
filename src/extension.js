/*********************************************************************
 * Shortcuts is Copyright (C) 2016-2018 Kyle Robbertze
 * African Institute for Mathematical Sciences, South Africa
 *
 * Shortcuts is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Shortcuts is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Shortcuts.  If not, see <http://www.gnu.org/licenses/>.
 **********************************************************************/

import GLib from "gi://GLib";
import Gio from "gi://Gio";
import Shell from "gi://Shell";
import St from "gi://St";
import Cogl from "gi://Cogl";
import Meta from "gi://Meta";
import Clutter from "gi://Clutter";

import {
    Extension,
    gettext as _,
} from "resource:///org/gnome/shell/extensions/extension.js";

import * as Main from "resource:///org/gnome/shell/ui/main.js";

export default class Shortcuts extends Extension {
    /*
     * Enables the plugin by adding listeners and icons as necessary
     */
    enable() {
        this._settings = this.getSettings();
        this._settingSignals = [];

        const settingsToConnect = [
            { key: "show-icon", method: this._toggleIcon },
            { key: "use-custom-shortcuts", method: this._setShortcutsFile },
            { key: "use-transparency", method: this._setTransparency },
            { key: "visibility", method: this._setTransparency },
            {
                key: "enable-appspecific-files",
                method: this._setEnableAppspecific,
            },
        ];

        settingsToConnect.forEach(({ key, method }) => {
            this._settingSignals.push(
                this._settings.connect(`changed::${key}`, method.bind(this))
            );
        });
        this._isAdded = false;
        // eslint-disable-next-line no-unused-vars
        Main.overview._specialToggle = (evt) => {
            this._toggleShortcuts();
        };

        Main.wm.addKeybinding(
            "shortcuts-toggle-overview",
            this._settings,
            Meta.KeyBindingFlags.IGNORE_AUTOREPEAT,
            Shell.ActionMode.NORMAL | Shell.ActionMode.OVERVIEW,
            this._toggleShortcuts.bind(this)
        );

        // Initialize settings
        this._setEnableAppspecific();
        this._setTransparency();
        this._setShortcutsFile();
        this._toggleIcon();
    }

    /**
     * Removes all traces of the listeners and icons that the extension created
     */
    disable() {
        //Remove setting Signals
        this._settingSignals.forEach(function (signal) {
            this._settings.disconnect(signal);
        }, this);
        this._settingSignals = null;
        this._settings = null;
        if (this._isAdded) {
            Main.panel._rightBox.remove_child(this._button);
            this._button = null;
            this._isAdded = false;
        }
        Main.wm.removeKeybinding("shortcuts-toggle-overview");
        delete Main.overview._specialToggle;
        this._visible = false;
    }
    /**
     * Builds the pop-up and shows the shortcut description list
     */
    _toggleShortcuts() {
        if (!this._visible) {
            if (!this._stage) {
                // Show popup
                this._columns = this._settings.get_int("maxcolumns");
                this._stage = new St.BoxLayout({
                    style_class: "background-boxlayout",
                    vertical: true,
                });
                this._main_panel = new St.BoxLayout({
                    style_class: "panel-boxlayout",
                    vertical: false,
                });

                this._stage.add_child(this._main_panel);

                this._child1_panel = new St.BoxLayout({
                    style_class: "child-boxlayout",
                    vertical: true,
                });
                this._child2_panel = new St.BoxLayout({
                    style_class: "child-boxlayout",
                    vertical: true,
                });
                this._main_panel.add_child(this._child1_panel);
                this._main_panel.add_child(this._child2_panel);
                if (this._columns >= 3) {
                    this._child3_panel = new St.BoxLayout({
                        style_class: "child-boxlayout",
                        vertical: true,
                    });
                    this._main_panel.add_child(this._child3_panel);
                }
                if (this._columns >= 4) {
                    this._child4_panel = new St.BoxLayout({
                        style_class: "child-boxlayout",
                        vertical: true,
                    });
                    this._main_panel.add_child(this._child4_panel);
                }
                if (this._columns === 5) {
                    this._child5_panel = new St.BoxLayout({
                        style_class: "child-boxlayout",
                        vertical: true,
                    });
                    this._main_panel.add_child(this._child4_panel);
                }
                this._readShortcuts();

                this._stage.add_child(
                    new St.Label({
                        style_class: "superkey-prompt",
                        text: _(
                            "The super key is the Windows key on most keyboards"
                        ),
                    })
                );

                Main.uiGroup.add_child(this._stage);
            }

            let color = new Cogl.Color({
                red: 10,
                blue: 10,
                green: 10,
                alpha: 255,
            });
            color.alpha = 255 * this._visibility;
            this._stage.set_background_color(color);

            let monitor = Main.layoutManager.primaryMonitor;

            this._stage.set_position(
                monitor.x +
                    Math.floor(monitor.width / 2 - this._stage.width / 2),
                monitor.y +
                    Math.floor(monitor.height / 2 - this._stage.height / 2)
            );
            this._visible = true;
        } else {
            // Hide popup
            this._hideShortcuts();
        }
    }

    _getPanel2columns(listProgress) {
        return listProgress < 0.5 ? this._child1_panel : this._child2_panel;
    }

    _getPanel3columns(listProgress) {
        const panels = [
            this._child1_panel,
            this._child2_panel,
            this._child3_panel,
        ];
        const index = Math.min(Math.floor(listProgress * 3), 2);
        return panels[index];
    }

    _getPanel4columns(listProgress) {
        const panels = [
            this._child1_panel,
            this._child2_panel,
            this._child3_panel,
            this._child4_panel,
        ];
        const index = Math.min(Math.floor(listProgress * 4), 3);
        return panels[index];
    }

    _getPanel5columns(listProgress) {
        const panels = [
            this._child1_panel,
            this._child2_panel,
            this._child3_panel,
            this._child4_panel,
            this._child5_panel,
        ];
        const index = Math.min(Math.floor(listProgress * 5), 4);
        return panels[index];
    }

    _getPanel(listProgress) {
        switch (this._columns) {
            case 2:
                return this._getPanel2columns(listProgress);
            case 3:
                return this._getPanel3columns(listProgress);
            case 4:
                return this._getPanel4columns(listProgress);
            case 5:
                return this._getPanel5columns(listProgress);
            default:
                Main.notifyError(_("Invalid number of columns"));
                return null;
        }
    }

    _getAppIDjson(APP_ID) {
        let systempath = "/etc/" + this.uuid + "/" + APP_ID;
        let userpath =
            GLib.get_user_config_dir() + "/" + this.uuid + "/" + APP_ID;
        if (GLib.file_test(userpath, GLib.FileTest.EXISTS)) {
            return userpath;
        }
        if (GLib.file_test(systempath, GLib.FileTest.EXISTS)) {
            return systempath;
        }
        let msg = _(
            "Application spedific Shortcuts file not found: '%s'"
        ).format(APP_ID);
        console.log(msg);
        return "";
    }
    /**
     * Reads the shortcuts from a file specified in the settings. If this is not
     * there then it defaults to the shortcuts file provided by the extension.
     */
    _readShortcuts() {
        let SHORTCUTS_FILE = this._settings.get_boolean("use-custom-shortcuts")
            ? this._settings.get_string("shortcuts-file")
            : this.dir.get_child("shortcuts.json").get_path();
        if (this._appspecific) {
            let APP_ID = null;
            let APP_JSON = "";
            const focusApp = Shell.WindowTracker.get_default().focus_app;
            if (focusApp) {
                APP_ID = focusApp.id;
                APP_JSON = this._getAppIDjson(APP_ID.slice(0, -8) + ".json");
                if (APP_JSON !== "") {
                    SHORTCUTS_FILE = APP_JSON;
                }
            }
        }
        if (!GLib.file_test(SHORTCUTS_FILE, GLib.FileTest.EXISTS)) {
            let msg = _("Shortcuts file not found: '%s'").format(
                SHORTCUTS_FILE
            );
            Main.notifyError(msg);
            return;
        }
        let file = Gio.file_new_for_path(SHORTCUTS_FILE);
        let [result, contents] = file.load_contents(null);
        if (!result) {
            let msg = _("Unable to read file: '%s'").format(SHORTCUTS_FILE);
            Main.notifyError(msg);
            return;
        }

        let shortcuts = JSON.parse(new TextDecoder().decode(contents));
        let shortcutLength = shortcuts.length;
        for (const scelement of shortcuts) {
            shortcutLength += scelement.shortcuts.length;
        }

        let listProgress = 0.0;
        for (const i_element of shortcuts) {
            listProgress += (i_element.shortcuts.length * 1.0) / shortcutLength;
            let panel = this._getPanel(listProgress);
            panel.add_child(
                new St.Label({
                    style_class: "shortcut-section",
                    text: _(i_element.name),
                })
            );
            for (const j_element of i_element.shortcuts) {
                let item_panel = new St.BoxLayout({
                    style_class: "item-boxlayout",
                    vertical: false,
                });
                let key = _(j_element.key);
                let description = _(j_element.description);
                item_panel.add_child(
                    new St.Label({
                        style_class: "shortcut-key-label",
                        text: key,
                    })
                );
                item_panel.add_child(
                    new St.Label({
                        style_class: "shortcut-description-label",
                        text: description,
                    })
                );
                panel.add_child(item_panel);
            }
        }
    }

    /**
     * Removes the actors used to make the pop-up describing the shortcuts.
     */
    _hideShortcuts() {
        this._main_panel.remove_child(this._child1_panel);
        this._main_panel.remove_child(this._child2_panel);
        if (this._columns >= 3) {
            this._main_panel.remove_child(this._child3_panel);
        }
        if (this._columns >= 4) {
            this._main_panel.remove_child(this._child4_panel);
        }
        if (this._columns === 5) {
            this._main_panel.remove_child(this._child5_panel);
        }
        this._stage.remove_child(this._main_panel);
        Main.uiGroup.remove_child(this._stage);
        this._child1_panel = null;
        this._child2_panel = null;
        this._child3_panel = null;
        this._child4_panel = null;
        this._child5_panel = null;
        this._main_panel = null;
        this._stage = null;
        this._visible = false;
    }

    /*
     * Shows or hides the icon in the right box of the top panel as the user
     * changes the setting
     */
    _toggleIcon() {
        let SHOW_ICON = this._settings.get_boolean("show-icon");
        if (!SHOW_ICON) {
            if (this._isAdded) {
                Main.panel._rightBox.remove_child(this._button);
                this._isAdded = false;
            }
            return;
        }
        if (!this._isAdded) {
            this._button = new St.Bin({
                style_class: "panel-button",
                reactive: true,
                can_focus: true,
                x_expand: true,
                y_expand: false,
                track_hover: true,
            });
            let icon = new St.Icon({
                icon_name: "preferences-desktop-keyboard-shortcuts-symbolic",
                style_class: "system-status-icon",
            });

            this._button.set_child(icon);
            this._button.connect(
                "button-press-event",
                this._onButtonPress.bind(this)
            );

            Main.panel._rightBox.insert_child_at_index(this._button, 0);
            this._isAdded = true;
        }
    }
    _onButtonPress(actor, event) {
        let button = event.get_button();
        if (button === 1 || button === 3) {
            // Left click and right click
            this._toggleShortcuts();
        } else if (button === 2) {
            // Middle click
            this.openPreferences();
        }
        return Clutter.EVENT_STOP;
    }

    /**
     * Updates the shortcut file location when it is changed in the settings
     */
    _setShortcutsFile() {
        if (!this._settings.get_boolean("use-custom-shortcuts")) {
            this._settings.set_string(
                "shortcuts-file",
                this.dir.get_child("shortcuts.json").get_path()
            );
        }
    }

    _setTransparency() {
        if (this._settings.get_boolean("use-transparency")) {
            this._visibility = this._settings.get_int("visibility") / 100;
        } else {
            this._visibility = 1;
        }
    }

    _setEnableAppspecific() {
        this._appspecific = this._settings.get_boolean(
            "enable-appspecific-files"
        );
        if (this._appspecific) {
            const path = GLib.get_user_config_dir() + "/" + this.uuid;
            const exitCode = GLib.mkdir_with_parents(path, 493);
            if (exitCode === -1) {
                let msg = _("Failed to create directory: '%s'").format(path);
                console.log(msg);
            }
        }
    }
}
