#!/bin/bash

set -e

if [ "$UID" = "0" ]; then
    echo 'This should not be run as root'
    exit 101
fi

NAME=Shortcuts\@kyle.aims.ac.za

function compile-translations {
    echo 'Compiling translations...'
    for po in locale/*/LC_MESSAGES/*.po; do
        msgfmt -cv -o ${po%.po}.mo $po;
    done
}

function compile-preferences {
    if [ -d src/schemas ]; then
        echo 'Compiling preferences...'
        glib-compile-schemas --targetdir=src/schemas src/schemas
    else
        echo 'No preferences to compile... Skipping'
    fi
}

function make-local-install {
    DEST=~/.local/share/gnome-shell/extensions/$NAME

    echo 'Installing...'
    pack
    gnome-extensions install -f Shortcuts@kyle.aims.ac.za.shell-extension.zip
    rm Shortcuts@kyle.aims.ac.za.shell-extension.zip
    echo 'Done'
}

function make-zip {
    if [ -d build ]; then
        rm -r build
    fi
    mkdir build
    compile-translations
 #   compile-preferences
    echo 'Coping files...'
    cp -r LICENSE README.md src/* locale build/
    # https://gjs.guide/extensions/review-guidelines/review-guidelines.html#don-t-include-unnecessary-files
    rm build/translation.js
    rm build/locale/LINGUAS build/locale/Shortcuts.pot
    rm -f build/locale/*/LC_MESSAGES/*.po
    echo 'Creating archive...'
    cd build
    zip -r ../"$NAME".zip ./*
    cd ..
    rm -r build
    echo 'Done'
}

function pack {
    if [ -d po ]; then
        rm -r po
    fi
    mkdir po
    cp locale/af/LC_MESSAGES/Shortcuts.po po/af.po
    cp locale/de/LC_MESSAGES/Shortcuts.po po/de.po
    echo 'Packing extension to zip...'
    cd src
    gnome-extensions pack -f --podir=../po/ --out-dir=../ --extra-source=shortcuts.json --extra-source=../LICENSE
    cd ..
    rm -r po
}

function updatepotfile() {

    reffile=Shortcuts.pot
    xgettext --from-code=UTF-8 --output=locale/"$reffile" src/*.js src/schemas/*.xml
    echo "Done."
}

function usage() {
    echo 'Useage: ./install.sh COMMAND'
    echo 'COMMAND:'
    echo "  local-install  install the extension in the user's home directory"
    echo '                 under ~/.local'
    echo '  pack            Creates a zip file of the extension'
    echo '  updatepotfile   Update the pot file with new translations'
    echo '  help           Show this help message'
    exit 1
}

case "$1" in
    "local-install" )
        make-local-install
        ;;

    "zip" )
        make-zip
        ;;

    "pack" )
        pack
        ;;

    "updatepotfile" )
        updatepotfile
        ;;

    * )
        usage
        ;;
esac
exit
